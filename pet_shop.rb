def pet_shop_name(pet_shop)
return pet_shop[:name]
end


def total_cash(pet_shop)
    return pet_shop[:admin][:total_cash]
end

def add_or_remove_cash(pet_shop, amount)
  amont = 0
  pet_shop[:admin][:total_cash] += amount
end

def pets_sold(pet_shop)
return pet_shop[:admin][:pets_sold]
end


def increase_pets_sold(pet_shop, amount)
  pet_shop[:admin][:pets_sold] += amount
  return pet_shop[:admin][:pets_sold]
end

def stock_count(pet_shop)
pet_shop[:pets].length
end


def pets_by_breed(pet_shop, breed)
breed_counter = []

# read the key "breed"
for pet in pet_shop[:pets] # Reading the entire pets array
if pet[:breed] == breed     #pet is representing the specific pet in an array of pets and returns the entire key/value pet
  breed_counter.push(pet)
end
end
return breed_counter
end

def find_pet_by_name(pet_shop, name)
  for pet in pet_shop[:pets]
    if pet[:name] == name
      return pet
    end
  end
return nil
end

def remove_pet_by_name(pet_shop, name)
pet = find_pet_by_name(pet_shop, name)
pet_shop[:pets].delete(pet)
end

def add_pet_to_stock(pet_shop, new_pet)
  pet_shop[:pets] << new_pet
end

def customer_cash(customer)
return customer[:cash]
end

def remove_customer_cash(customer, amount)
customer[:cash] -= amount
end

def customer_pet_count(customer)
  customer[:pets].length
end

def add_pet_to_customer(customer, new_pet)
customer[:pets] << new_pet
end

def customer_can_afford_pet(customers, new_pet)

  for customer in customers
    if customers[:cash] >= 100.0
    return true
    else
  return false
    end
  end
end

def sell_pet_to_customer(pet_shop, pet, customer)

if pet && customer[:cash] >= pet[:price]          # If in doubt start printing
  customer[:pets].push(pet)                       # I'm adding an oject into an array. 1st assertion.
  pet_shop[:admin][:pets_sold] += 1               
  customer[:cash] -= pet[:price]
  pet_shop[:admin][:total_cash] += pet[:price]
else
return nil
end

end
